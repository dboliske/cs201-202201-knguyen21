package labs.lab5;

public class CTAStation extends GeoLocation{
// Create variables
	private String name;
	private String location; 
	private boolean wheelchair;
	private boolean open;
	 
	
	// Using different methods
	public CTAStation() {
		
		this.name = "";
		this.location = "";
		this.wheelchair = false;
		this.open = false;
	}
	  
	public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open) {
    super(lat,lng);
       this.name= name;
       this.location = location;
	   this.wheelchair = wheelchair;
	   this.open= open;
	}
	   
	public void setName(String name) {
		   this.name = name;
		}
	   
	   
	   public String getName() {
		  return name;
		}
	   
	   
	   public void setLocation(String location) {
		   this.location = location;
		}
	   
	   public String getLocation() {
		   return location;
	   }
	   
	   
		
	   public void setWheelchair(boolean wheelchair) {
			this.wheelchair = wheelchair;
		
		}
	   
	   
	   public boolean hasWheelchair() {
			return wheelchair;
		}	
		
		
	   public void setOpen(boolean open) {
			this.open= open;
		}  
		
	   
		public boolean isOpen() {
			return open;
		}
		
		
	   // toString format
		@Override
		public String toString() {
return "Station name: " + name +", Location: " + location + " Wheelchair access: " + wheelchair + "Open: " + open;
		}
		
		
		
}
