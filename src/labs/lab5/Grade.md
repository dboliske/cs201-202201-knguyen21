# Lab 5

## Total

20/20

## Break Down

CTAStation

- Variables:                    2/2
- Constructors:                 1/1
- Accessors:                    2/2
- Mutators:                     2/2
- toString:                     1/1
- equals:                       2/2

CTAStopApp

- Reads in data:                2/2
- Loops to display menu:        2/2
- Displays station names:       1/1
- Displays stations by access:  2/2
- Displays nearest station:     2/2
- Exits                         1/1

## Comments
For your default constructor, rememeber to also call the super() default constructor as well.

Remember to format outputs so that they are easy to read for the user.

Also remember to document for good practice.


