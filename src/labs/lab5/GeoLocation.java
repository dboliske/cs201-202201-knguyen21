package labs.lab5;


public class GeoLocation {
	
	// Part 1: create variables:
		private double lat;
		   private double lng;
		   
		   
		   GeoLocation() {
		       super();
		   }
		  
		   //non-default constructor which takes lat and lng as arguments.
		   public GeoLocation(double lat, double lng) {
		       super();
		       this.lat = lat;
		       this.lng = lng;
		   }
	      // For lat:
		   //accessor method 
		   public double getLat() {
		       return lat;
		   }
		   //mutator method 
		   public void setLat(double lat) {
		       this.lat = lat;
		   }
		  // For lng
		   //Accessor method 
		   public double getLng() {
		       return lng;
		   }
		  
		   //mutator method 
		   public void setLng(double lng) {
		       this.lng = lng;
		   }
		  
		   //toString() method . It is used to print the object in desired format
		   @Override
		   public String toString() {
		       return "("+lat + "," + lng + ")";
		   }
		  
		   //checks lat whether it is between -90 and 90 and return true if it is.
		   public boolean validLat()
		   {
		       if(this.lat>=-90 && this.lat<=90)
		           return true;
		       return false;
		   }
		  
		   //checks lng whether it is between -180 and 180 and return true if it is.
		   public boolean validLng()
		   {
		       if(this.lng>=-180 && this.lng<=180)
		           return true;
		       return false;
		   }
		   
		   public double calcDistance(GeoLocation pos) {

		        double distance = Math.sqrt(Math.pow(this.lat - pos.lat, 2) + Math.pow(this.lng - pos.lng, 2));

		        return distance;

		    }

		    public double calcDistance(double lat1, double long1) {

		        double distance = Math.sqrt(Math.pow(lat1 - this.lat, 2) + Math.pow(long1 - this.lng, 2));

		        return distance;}
		   
	// Part 9: Write a method that will compare this instance to another GeoLocation (the equals method).
		   @Override
		   public boolean equals(Object obj) {
			   if (!(obj instanceof GeoLocation)) {
				   return false;
			   }
			   else {
			   GeoLocation g = (GeoLocation)obj;
			   if(this.lat ==(g.getLat())&& this.lng ==g.getLng())
				   return true;
			   else {
				   return false;}
			   }
			   }
                }
		   
			
		


