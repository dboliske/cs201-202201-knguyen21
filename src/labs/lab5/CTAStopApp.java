package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class CTAStopApp {

	public static CTAStation[] readFile(String filename ) {
		CTAStation[] stations = new CTAStation[34];
		int count = 0;
		
	try {
		File f = new File(filename);
		Scanner input = new Scanner(f);

	// Read file, take in data and convert to double:	
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String[] values = line.split(",");
			CTAStation a = null;
			if(!values[1].equals("Latitude")) {
				a = new CTAStation(values[0],
						Double.parseDouble(values[1]), 
						Double.parseDouble(values[2]),
						values[3],
						Boolean.parseBoolean(values[4]),
						Boolean.parseBoolean(values[5]));
				stations[count] = a;
				count++;
			}
			
			
		}
		
	}catch(FileNotFoundException fnf) {
		
	}

	return stations;
	}
	
		public static CTAStation[] menu(Scanner input, CTAStation[] data) {
			boolean done = false;
			// Menu for user to choose
			do {
				System.out.println("1. Display station names");
				System.out.println("2. Display station with/ without wheelchair access");
				System.out.println("3. Display nearest station");
				System.out.println("4. Exit");
				System.out.print("Choice: ");
				String choice = input.nextLine();
				
				switch (choice) {
					case "1": // Display station names
						 displayStationNames(data);
						break;
					case "2": // Display stations with wheelchair/ without
						 displayByWheelchair(data);
						break;
					case "3": // Nearest station
						displayNearest(data);
						break;
					case "4": // Exit
						done = true;
						break;
					default:
						System.out.println("Sorry the input is not valid.");
				}
			} while (!done);
			
			return data;
		}
		
		// Option 1:
		public static void displayStationNames(CTAStation[] data) {
		
		for(int i = 0; i < data.length; i++) {
			System.out.println(data[i]);
			}
		
		
		}  // Option 2:
		public static void displayByWheelchair(CTAStation[] data) {
			int x = 0;
		Scanner input=new Scanner(System.in);
		System.out.println("Wheelchair accessibility? ('y' or 'n'):");
		String a=input.nextLine();
		
		// Keep asking until user enter valid input
		while (x!=0) {
			System.out.println("Wheelchair accessibility? ('y' or 'n'):");
			a =input.nextLine();}
		
		
		if(a.equalsIgnoreCase("y"))    // If user enters 'y'
			
		{for(int i = 0; i < data.length; i++) {
			if(data[i].hasWheelchair() == true) {
				System.out.println(data[i]);}
		}

         x=0;
         } else if(a.equalsIgnoreCase("n")) { // If user enters 'n'
	for(int i = 0; i < data.length; i++) {
		if(data[i].hasWheelchair() == false) {
			System.out.println(data[i]);	
		}
	}
             x=0;}
else {x=1;
}		
		}		
		
	// Option 3:	
		public static void displayNearest(CTAStation[] data) {
			
Scanner input2=new Scanner(System.in);
System.out.print("Enter longtitude:");		
double long1= Double.parseDouble(input2.nextLine());		
System.out.print("Enter lattitude:");	
double lat1= Double.parseDouble(input2.nextLine());	

double min = data[0].calcDistance(lat1, long1);
CTAStation nearestStation = data[0]; 
for(int i = 0; i < data.length; i++) {

	double nearest= data[i].calcDistance(lat1, long1);
	if (nearest < min) {
		min = nearest;
		nearestStation = data[i];
	}
}
			
			System.out.print(nearestStation.toString());}		
		
		
		
		
		// For reading the file (user type in the file path)
		public static void main(String[] args) {
			Scanner input = new Scanner(System.in);
			System.out.print("Load file: ");
			String filename = input.nextLine();
			// Load file (if exists)
			CTAStation[] data = readFile(filename);
			// Menu
			data = menu(input, data);
			// Save file
			
			input.close();
			System.out.println("Ended!");
		}
		
		
		
				
		
		
}