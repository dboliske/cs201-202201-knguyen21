package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

//importing java utility class
import java.util.*;

//Main class
public class DeliCLient
{
  
  //initializing scanner 
  static Scanner sc = new Scanner(System.in);
  
  //addCustomer method which takes an ArrayList as an argument
  public int addCustomer(ArrayList<String> customerQueue){
      //Prompt customer for name
      System.out.print("Enter customer's name: ");
      
      //Additional sc.nextLine()  to break the sc.nextInt()
      sc.nextLine();
      
      //Taking input the name of the customer
      String name = sc.nextLine();
      
      //Adding customer to the end of the queue
      customerQueue.add(name);
      
      //Return customer position 
      return customerQueue.size();
      
  }
  
  //method helpCustomer takes ArrayList as an argument
  public void helpCustomer(ArrayList<String> customerQueue){
      
      //removing first customer from the queue
      String customerName = customerQueue.remove(0);
      
      //printing customer's name
      System.out.println("Customer being served: " + customerName);
  }
  
  //Creating menu
  //takes an ArrayList as an argument
  public void menu(ArrayList<String> customerQueue){
      
      //initializing confirm with 0
      int confirm = 0;
      
      //To keep on asking the user
      while(confirm != 3){
          
          //print menu
          System.out.println("1. Add customer to queue");
              System.out.println("2. Help customer");
                  System.out.println("3. Exit");
                  System.out.print("Enter your choice: ");
          
          //taking user's choice input
          int choice = sc.nextInt();
      
          //Option 1
          if(choice == 1){
          addCustomer(customerQueue);
          confirm = 1;
          
          }
          
          //Option 2
          else if(choice == 2){
              helpCustomer(customerQueue);
              confirm = 2;
          }
          
          //Option 3
          else if(choice == 3){
              System.out.println("See you next time!");
              confirm = 3;
          } //If user enters anything other than 1,2,3
          else {System.out.println("Please inter valid input!");confirm = 4;}
      
      }
  }
  
      // If user enters 3, confirm = 3 and loop ends
  
  
  
  

      public static void main(String[] args) {
          
          //Creating  ArrayList
          ArrayList<String> customerQueue = new ArrayList<String>();
          
          //Create instance for DeliCLient
      DeliCLient deli = new DeliCLient();
      
      //Calling the menu
      deli.menu(customerQueue);
      
  }
}