# Lab 0

## Total

19.5/20

## Break Down

* Eclipse "Hello World" program         5/5
* Correct TryVariables.java & run       3.5/4
* Name and Birthdate program            5/5
* Square
  * Pseudocode                          2/2
  * Correct output matches pseudocode   2/2
* Documentation                         2/2

## Comments
Line 10 in your TryVariables exercise should be short not byte