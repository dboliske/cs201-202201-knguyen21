package labs.lab0;

import java.util.Scanner;

public class KhoiNguyenStep56789 {

	public static void main(String[] args) {
		
//Exercise 5: Pseudocode to output a square (hollow)
//1: Create scanner to prompt user for pattern of the square (#,+,...) and the size of the square (number of patterns per side)
//2: Create column (i) from 1 to the number chosen by user
//3: Use for loop for (j) to fill out the line with pattern chosen by user, this will result in the formation of top and bottom rows of the square
//4: For the middle part of the square, use for loop and if/else for (k) to make the square hollow.		 
	
		// Exercise 7: The program:
		
		String pattern;
	        int size;

	        Scanner input = new Scanner(System.in);
            // Create scanner for user to input information
	        
	        System.out.print("Choose pattern for the square(+,*,...) : ");
	        pattern = input.nextLine();

	        System.out.print("Enter square's size (no.of patterns per side) : ");
	        size = input.nextInt();

	        input.close();
	        
	        for(int i=1; i<= size; i++) {       
	            
	            System.out.println();

	            if(i==1 || i == size) { // fill the top row (i== 1) and bottom row (i== size) with chosen pattern
	      
	                for(int j=1; j <= size; j++){
	                
	                    System.out.print(pattern + " ");
	           // Print the chosen pattern for the entire row     
	                }
	            } 
                  else { 
	                // This is for the middle part of the square (all rows and columns except for top and bottom row)
	                for(int k= 1; k<= size;k++) { 
	                
	                    if(k== 1 || k == size) { 
	                    
	                        System.out.print(pattern + " ");
	              // This is to print the pattern at the first and last position of k      
	                    }     
                               
	                    else {      
	                        System.out.print("  ");
	          // Except for k=1 and k= user input size, the other positions will be filled with blank spaces, hence make the square hollow.
	                    }
	                }
              }
	        }
	}
}
// Exercise 8:The program gave desired results (the square was printed with pattern chosen by user and the number of patterns in each side of the square is equal to each other)
// Exercise 9:No changes were needed since the program yields desired results
