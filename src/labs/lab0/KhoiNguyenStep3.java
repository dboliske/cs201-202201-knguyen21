package labs.lab0;

public class KhoiNguyenStep3 {

	public static void main(String[] args) {
		// I commented my changes to the following code below

				int integer = 100;
				long large = 42561230L;
				byte small = 25; // 'byte' was added to assign number to variable
				byte tiny = 19;

				float f = .0925F;
			    double decimal = 0.725;
				double largeDouble = +6.022E23; // This is in scientific notation

				char character = 'A';
				boolean t = true;

				// One 's' in System has not been capitalized
				// The ',' replaced with '.'
				System.out.println("integer is " + integer);
				System.out.println("large is " + large);
				System.out.println("small is " + small);
				System.out.println("tiny is " + tiny); //'Tine' replaced with 'tiny'
				System.out.println("f is " + f);
				System.out.println("decimal is " + decimal);
				System.out.println("largeDouble is " + largeDouble);
				System.out.println("character is " + character); //'+ character'
				System.out.println("t is " + t);
		// TODO Auto-generated method stub

	}

}
