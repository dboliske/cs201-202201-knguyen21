package labs.lab1;
import java.util.Scanner;
public class KhoiNguyenExercise4 {

	public static void main(String[] args) {
		
		
		// Exercise 4:
		
	Scanner input = new Scanner(System.in);
    // Create scanner for user to input information
		
	System.out.print("Enter Farenheit temperature : ");
    double farentemp = input.nextDouble();
		
    System.out.print("Enter Celcius temperature : ");
    double celtemp = input.nextDouble();
		
     input.close();
        
    double fartocel = ((farentemp-32)*5/9);
    double celtofar = ((celtemp*9/5)+32);
        
    System.out.println(farentemp + " Farenheit is equal to " + fartocel + " Celcius");
    System.out.println(celtemp + " Celcius is equal to " + celtofar + " Farenheit");
    
    
    // TEST PLAN
    
  // Trial 1:
  // Temperature                    Expected         Program result
  // Fahrenheit:32             Celcius:0                   0
  // Celcius:22               Fahrenheit:71.6             71.6
  
  // Trial 2:
 // Temperature                    Expected         Program result
 // Fahrenheit:-20             Celcius:-28.8889         -28.8889
 // Celcius:-30               Fahrenheit:-22             -22
    
 // Trial 3:
 // Temperature                    Expected         Program result
 // Fahrenheit:11.2             Celcius:-11.556         -11.556
 // Celcius:22.4               Fahrenheit:72.32          72.32   
    
    
	}

}
