package labs.lab1;
import java.util.Scanner;
public class KhoiNguyenExercise5 {

	public static void main(String[] args) {
		
		 // Exercise 5:
		
		Scanner input = new Scanner(System.in);
        // Create scanner for user to input information
		
		System.out.print("Enter length of box in inches : ");
        double length = input.nextDouble();
		
        System.out.print("Enter width of box in inches : ");
        double width = input.nextDouble();
		
        System.out.print("Enter depth of box in inches: ");
        double depth = input.nextDouble();
		
        input.close();
        double lengthfeet = length /12;
        double widthfeet = width  /12;
        double depthfeet = depth /12;
        // convert inches to feet
        
        // Calculate surface area = amount of wood
        double surfacearea = 2*((lengthfeet*widthfeet) + (lengthfeet*depthfeet) + (widthfeet*depthfeet));
        
        System.out.println(" Amount of wood needed is " + surfacearea + " square feet");
        
        
     // TEST PLAN
        
        // Trial 1:
        // Parameters     Expected amount of wood(square feet)        Program result
        // Length: 20                4.8611                              4.8611
        // Width: 10                           
        // Depth: 5
        
        
        // Trial 2:
        // Parameters     Expected amount of wood(square feet)        Program result
        // Length: 35                10.764                              10.7639
        // Width: 15                           
        // Depth: 5
       
        
        // Trial 3:
        // Parameters     Expected amount of wood(square feet)        Program result
        // Length: 20.5                5.33                              5.328
        // Width: 10.3                           
        // Depth: 5.6 
        
	}

}
