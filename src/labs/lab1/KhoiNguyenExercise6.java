package labs.lab1;
import java.util.Scanner;
public class KhoiNguyenExercise6 {

	public static void main(String[] args) {
		
		// Exercise 6:
		
		 Scanner input = new Scanner(System.in);

	        System.out.print("Input a value for inch: ");
	     
	        double inch = input.nextDouble();
	        input.close();
	        double centimeters = inch * 2.54;
	        System.out.println(inch + " inch is equal to " + centimeters + " centimeters");

	        // TEST PLAN
	        
	        // Trial 1:
	        // Inches                   Expected(cm)        Program result
	        //     10                       25.4                 25.4
	        
	        
	     // Trial 2:
	        // Inches                   Expected(cm)        Program result
	        //     65                       165.1                 165.1
	        
	     // Trial 3:
	        // Inches                   Expected(cm)        Program result
	        //     10.9                       27.686                 27.686
	        
	}

}
