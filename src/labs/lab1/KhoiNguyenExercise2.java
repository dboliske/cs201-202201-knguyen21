package labs.lab1;

public class KhoiNguyenExercise2 {

	public static void main(String[] args) {
		
		// Exercise 2
		
		
		int myAge = 20;
		// My age
		int FatherAge = 45;
		// My father's age
		int myBirthyear = 2001;
		// My birth year
		int FatherminusMe = FatherAge - myAge;
		// My age subtracted from my father's
		System.out.println("My age subtracted from my father's is: " + FatherminusMe);
		
		int doubleBirthyear = myBirthyear *2;
		// My birth year times 2
		System.out.println("My birthyear times 2 is: " + doubleBirthyear);
		
		int myHeightInch = 68;
		// My height in inches
		
		double myHeightCm = myHeightInch * 2.54;
		// My height converted to cm
		System.out.println("My height is: " + myHeightInch + " inches and in cm is: " + myHeightCm);
		
		// My height converted to feet and inches
		int feet = myHeightInch / 12;
		int inches = myHeightInch % 12;
		
		
		System.out.println("My height is: "+ feet + " feet " + inches + " inches");
		

	}

}
