package labs.lab4;

public class PhoneNoApp {

	public static void main(String[] args) {
		
		
		// Part 10:
		
		//Creating object using default constructor
	       PhoneNumber phonenumber1=new PhoneNumber();
	       phonenumber1.setCountryCode("1"); //setting values using mutator method
	       phonenumber1.setAreaCode("312"); 
	       phonenumber1.setNumber("5697381");
	      
	       //Assigning values for name and strength to create new phone number 2
	       String countryCode="1";
	       String areaCode = "312";
	       String number = "6879053";
	      
	       //Creating object using non-default constructor
	       PhoneNumber phonenumber2 =new PhoneNumber(countryCode,areaCode,number);
	       
	       // Display using toString:
	       
	       System.out.println("Phonenumber 1:");
		      
	       System.out.println(phonenumber1.toString());
	       
	       System.out.println("Phonenumber 2:");
		      
	       System.out.println(phonenumber2.toString());

	}

}
