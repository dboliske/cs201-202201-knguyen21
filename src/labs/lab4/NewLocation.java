package labs.lab4;

public class NewLocation {

	// Part 10:
	public static void main(String[] args) {
		//Creating object using default constructor
	       GeoLocation location1=new GeoLocation();
	       location1.setLat(82.1); //setting values using mutator method
	       location1.setLng(150.7); 
	      
	       //Assigning values for lat and lng to create new GeoLocation 2
	       double lat=78.5;
	       double lng=56.7;
	      
	       //Creating object using non-default constructor
	       GeoLocation location2=new GeoLocation(lat,lng);
	      
	       System.out.println("Location 1:");
	      
	       System.out.println("("+ location1.getLat()+","+ location1.getLng()+")");
	      
	       System.out.println("\nLocation 2:");
	      
	       System.out.println("("+ location2.getLat()+","+ location2.getLng()+")");
	}

}
