package labs.lab4;

public class Potion {

	   // Create two instance variables, name (a String) and strength (a double).
	   private String name;
	   private double strength;
	  
	   
	   Potion() {
	       super();
	   }
	  
	   //non-default constructor which takes name and strength as arguments.
	   public Potion(String name, double strength) {
	       super();
	       this.name = name;
	       this.strength = strength;
	   }

	   //Accessor method for name
	   public String getName() {
	       return name;
	   }
	   //mutator method for name
	   public void setName(String name) {
	       this.name = name;
	   }
	  
	   //Accessor method for strength
	   public double getStrength() {
	       return strength;
	   }
	  
	   //mutator method for strength
	   public void setStrength(double strength) {
	       this.strength = strength;
	   }
	  
	   //toString() method . It is used to print the object in desired format
	   @Override
	   public String toString() {
	       return "Name:"+ name + ", Power: " + strength ;
	   }
	  
	   //checks strength whether it is between 0 and 10 and return true if it is.
	   public boolean checkStrength()
	   {
	       if(this.strength>=0 && this.strength<=10)
	           return true;
	       return false;
	   }
	  
	// Part 9: Write a method that will compare this instance to another Potion (the equals method).
		 @Override
		   public boolean equals(Object obj) {
			   if (!(obj instanceof Potion)) {
				   return false;
			   }
			   else {
			   Potion p = (Potion)obj;
			   if(this.name ==(p.getName())&& this.strength ==(p.getStrength()))
				   return true;
			   else {
				   return false;}
			
			   }
		 }
		
	}





