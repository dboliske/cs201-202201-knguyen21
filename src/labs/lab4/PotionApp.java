package labs.lab4;

public class PotionApp {

	public static void main(String[] args) {
		
		// Part 10:
		//Creating object using default constructor
	       Potion potion1=new Potion();
	       potion1.setName("Incendiary"); //setting values using mutator method
	       potion1.setStrength(9.7); 
	      
	       //Assigning values for name and strength to create new potion 2
	       String name="Corrossive";
	       double strength = 6.7;
	      
	       //Creating object using non-default constructor
	       Potion potion2=new Potion(name,strength);
	       
	       // Display using toString:
	       
	       System.out.println("Potion 1:");
		      
	       System.out.println(potion1.toString());
	       
	       System.out.println("Potion 2:");
		      
	       System.out.println(potion2.toString());

	}

}
