package labs.lab4;

public class PhoneNumber {
	
	
	
	   // Part 1 create 3 variables
	private String countryCode,areaCode,number;
	
	   // Part 2
	public PhoneNumber()
	{   this.countryCode = "";
	this.areaCode = "";
	this.number = "";
	}
	  // Part 3
	public PhoneNumber(String countryCode, String areaCode, String number) {

	   this.countryCode = countryCode;
	   this.areaCode = areaCode;
	   this.number = number;
	}

	// Part 4: Write 3 accessor methods, one for each instance variable.
	public String getCountryCode() {
	   return countryCode;
	}
	public String getAreaCode() {
	   return areaCode;
	}
	public String getNumber() {
	   return number;
	}
	// Part 5: Write 3 mutator methods, one for each instance variable.
	public void setCountryCode(String countryCode) {
	   this.countryCode = countryCode;
	}
	public void setAreaCode(String areaCode) {
	   this.areaCode = areaCode;
	}
	public void setNumber(String number) {
	   this.number = number;
	}
	// Part 6: Write a method that will return the entire phone number as a single string (the toString method).
	@Override
	public String toString() {
	   return "PhoneNumber: " + countryCode+"-" + areaCode + "-" + number;
	}

	// Part 7: Write a method that will return true if the areaCode is 3 characters long.
	public boolean isAreacode_3char()
	{if(areaCode.length()==3)
	   return true;
	return false;
	}

	// Part 8: Write a method that will return true if the number is 7 characters long.
	public boolean isnum_7char()
	{if(number.length()==7)
	   return true;
	return false;
	}
	
	// Part 9: Write a method that will compare this instance to another PhoneNumber (the equals method).
	 @Override
	   public boolean equals(Object obj) {
		   if (!(obj instanceof PhoneNumber)) {
			   return false;
		   }
		   else {
		   PhoneNumber p = (PhoneNumber)obj;
		   if(this.countryCode ==(p.getCountryCode())&& this.areaCode ==(p.getAreaCode())&& this.number ==(p.getNumber()))
			   return true;
		   else {
			   return false;}
		   }
		   }
	}