package labs.lab7;

public class SelectionSort {

public static void main(String[] args) {

double[] arr = { 3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282 }; // Initiate array

String comma = "";
System.out.print("Array before sorted: ");
for (int i = 0; i < arr.length; i++) {

System.out.print(comma + arr[i]);

comma = ", ";

}
System.out.println();
SelectionSort(arr);

String comma2 = "";
System.out.print("Array after sorted: ");

for (int i = 0; i < arr.length; i++) {

System.out.print(comma2 + arr[i]);

comma2 = ", ";

}

  // Complete the display with a closing ]

}

public static void SelectionSort(double[] arr) {

int i, j, min_id, n = arr.length;

for (i = 0; i < n - 1; i++) {

min_id = i;

for (j = i + 1; j < n; j++)

if (arr[j] < arr[min_id])

min_id = j;

// The found minimum element replace the first element

double temp = arr[min_id];

arr[min_id] = arr[i];

arr[i] = temp;

}

}

}