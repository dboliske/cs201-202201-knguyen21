package labs.lab7;

import java.util.Scanner;
public class BinarySearch {
  
// Returns position of x if it is present in arr[],
// else return -1
static int binarySearch(String[] arr, String x)
{
int l = 0, r = arr.length - 1;
while (l <= r) {
int m = l + (r - l) / 2;
  
int res = x.compareTo(arr[m]);
  
if (res == 0)   // Check if x is present at middle
return m;
  
if (res > 0) // If x greater then left half is ignored
l = m + 1;
  
else    // If x is smaller right half is ignored
r = m - 1;
}
return -1;
}
  

public static void main(String []args)
{
String[] arr = { "c", "html", "java", "python", "ruby", "scala"};// Initiate array
Scanner sc= new Scanner(System.in); 
System.out.print("Enter a string: "); // Prompt user for string to search
String str= sc.nextLine(); 
int result = binarySearch(arr, str);
  
if (result == -1)
System.out.println("Element not present"); // If the result cannot be found in the array
else
System.out.println("Element found at "  // If the result can be found (in this case it counts from 0)
+ "position " + result);
}
}
