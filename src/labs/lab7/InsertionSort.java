package labs.lab7;

public class InsertionSort
{
   public static void main(String[] args) { // Input the array of Strings
       String[] arr ={"cat", "fat", "dog", "apple", "bat", "egg"}; // Initialize array
int count = 0;
String sortedArray[] = sort_sub(arr, arr.length); // capture length of array
for(int i=0;i<sortedArray.length;i++){
System.out.println(sortedArray[i]);
}
}

public static  String[] sort_sub(String array[], int k){
String temporary=""; 
for(int i=0;i<k;i++){ // loop to run through
for(int j=i+1;j<k;j++){   
if(array[i].compareToIgnoreCase(array[j])>0){    
temporary = array[i];         // Swap position if condition is met
array[i]=array[j];
array[j]=temporary;
}
}
}
return array;
   }
}
