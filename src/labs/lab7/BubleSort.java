package labs.lab7;

public class BubleSort {
	public static void bubleSort(int[] arr) {
		int b = arr.length; //length of array passed to n
		int temp = 0; 
		  
		for(int i = 0; i < b; i++) { //loop to run in every pass
		for(int j=1; j < (b-i); j++) { 
		if(arr[j-1] > arr[j]) { 
		temp = arr[j-1]; //swap if the above condition is satisfied  
		arr[j-1] = arr[j];
		arr[j] = temp;
		}
		}
		}
		}
		public static void main(String[] args) { // Initialize array
		int arr[] = { 10, 4, 7, 3, 8, 6, 1, 2, 5, 9 };
		System.out.println("Array before applying Bubble Sort");

		for(int i = 0; i < arr.length; i++) { // Display array before sorting
		System.out.print(arr[i] + " ");
		}
		System.out.println();
		bubleSort(arr);
		System.out.println("Array after applying Bubble Sort");

		for(int i = 0; i < arr.length; i++) { // Display result after sorting
		System.out.print(arr[i] + " ");
		}
		}
}
