package labs.lab2;
import java.util.Scanner;
public class KhoiNguyenExercise3 {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);	
		// This scanner is for the first option
		Scanner input2 = new Scanner(System.in);	
		// This scanner is for the second and third option
		
		boolean cond = true; // Condition to run the while loop
		String option = "";
		
		
		//While loop will keep running as long as the condition is 'true'
		
		
		while(cond) { // Options for user to choose
			System.out.println("1: Say Hello");
			System.out.println("2: Addition");
			System.out.println("3: Multiplication");
			System.out.println("4: Exit program");
			option = input.nextLine();
			
			switch(option) {
			case "1": // Option 1
			System.out.println("Hello");
			break;
			case "2": // Option 2
				
// Enter the numbers, I used 'Double' so that decimals can be entered and present on console	
		   System.out.println("Enter 1st number");
		   double x = input2.nextDouble();
		   System.out.println("Enter 2nd number");
		   double y = input2.nextDouble();
		   double sum = x + y;
		   System.out.println("The sum for "+ x +" and " +y +" is " + sum);
		   break;
			case "3": // Option 3
		System.out.println("Enter 1st number");
		   x = input2.nextDouble();
	    System.out.println("Enter 2nd number");
		    y = input2.nextDouble();
		   double product = x * y;
		System.out.println("The product for "+ x +" and " +y +" is " + product);
		break;
		
			case "4": // To exit, cond is now false, hence stop the loop
				cond = false;
				break;
			}
		} System.out.println("Program exited");
	}

}
