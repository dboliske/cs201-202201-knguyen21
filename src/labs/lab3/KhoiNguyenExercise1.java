package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class KhoiNguyenExercise1 {

	public static void main(String[] args) throws IOException{
		
		
		File f = new File("src/labs/lab3/grades.csv");
		
		Scanner input = new Scanner(f);
		
		int numgrades = 14; // Number of values (grades)
		double total = 0; // Start total with 0 to add later on 
		while (input.hasNextLine()) {
			String current = input.nextLine();
	        String[] split = current.split(",");
	        total += Integer.parseInt(split[1]);
		}
		double ClassAverage = total / numgrades;
		// Calculating the average (double is chosen to present decimal values as well)
	    System.out.println("The class' average is " + ClassAverage);
		
		input.close();
		// TODO Auto-generated method stub

	}

}
