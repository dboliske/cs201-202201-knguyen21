package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class KhoiNguyenExercise2 {

	public static void main(String[] args) {
		
		
		
		int[] num=new int[100];//temporary array size to store numbers
		
		Scanner input=new Scanner(System.in);
		
		int i=0;//Count actual size of array
		while(true)
		{
		System.out.println("Enter the numbers, type Done to stop:");
		
		String a=input.nextLine();//In string form so that the program can recognize when user enters "Done"
		if(a.equals("Done"))
		{

		break;//if user enters done loop ends
		}
		else{
		num[i]=Integer.parseInt(a);//converting string inputs to number and add to the array
		}
		i++;
		}
		System.out.println("Enter the Filename:"); // Prompt user for filename
		String fileName = input.nextLine();
		
		try{
		FileWriter writer = new FileWriter(fileName); //open file to store input values
		for(int j=0; j<i; j++)
		{
		writer.write(num[j] + "\t" + "");//input values to file
		}
		writer.close();//closing the file
		}
		catch (IOException e) {
		e.printStackTrace();
		}
		input.close();
		
		// I found the saved txt file in the 'properties' section
	}

}
