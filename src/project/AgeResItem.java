package project;

public class AgeResItem extends ShelvedItem { // Constructor of age restricted Item
	
	public int age;
	   
	
	// Basic methods
	   
	   public AgeResItem() {
			
			this.age = 0;
			
			
		}	
	   
	   
	   public AgeResItem(String name, double price, int age) {
		   super(name,price);
		       this.age= age;
			}
			   
			public void setAge(int age) {
				   this.age = age;
				}
			   
			   
			   public int getAge() {
				  return age;}
				
			   
			   
				
			  	
				
			// toString format that will appear on console
				@Override
				public String toString() {
		return  "Name: " + getName() + ", price: " + getPrice() +  ", Restricted age: " + age;
				}
				

public boolean isValidAge() {
	if(this.age >=0) {
		return true;
	}
	else {
		return false;
	}
}
				
				
				
}



