package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;



public class GenstoreApp {
	
	
// This is to read the file and store it in an array
	public static ShelvedItem[] readFile(String filename ) {
		ShelvedItem[] item = new ShelvedItem[57]; // Initial array of size 57
		int count = 0;
		
		Scanner input1 = new Scanner(System.in);
		int choose = 0;
			
		
	try {
		File f = new File(filename);
		Scanner input2 = new Scanner(f);

	// Read file, take in data and convert to double:	
		while (input2.hasNextLine()) {
			try {
				String line = input2.nextLine();
				String[] values = line.split(",");
				ShelvedItem a = null;
				
				
				
	// Data with value length of 2 (only name and price) will be put as shelved item (choose =1)
		if(values.length == 2) { 
			choose = 1;
		}
		else {
			
// If the 3rd value have 10 spaces (Date) then it will be put as produced item			
			if(values[2].length() == 10) {
				choose = 2;
			}
			
// If the 3rd value have 2 spaces (restricted age) then it will be put as age-restricted item				
			else if(values[2].length()==2) {
				choose = 3;
			}
				
	// Change the format of data based on their type (shelved, produced or age-restricted)			
				switch (choose) {
				case 1:   // For shelved items
						a = new ShelvedItem(
							values[0],
							Double.parseDouble(values[1])
					);
					break;
				case 2:  // FOr produced items
					a = new ProducedItem(
							values[0],
							Double.parseDouble(values[1]),
							values[2])
						;
					break;
				case 3:  // For age-restricted items
					a = new AgeResItem(
							values[0],
							Double.parseDouble(values[1]),
							Integer.parseInt(values[2])
							
							);
					break;
			}
							
						
		// Resize the array to contain all the values	
				if (item.length == count) {
					item = resize(item, item.length*2);
				}
				
				item[count] = a;
				count++;
		}
			} catch (Exception e) {
				
			}
			
			
		}
		  // Catch to detect error while loading and reading file
	}catch(FileNotFoundException fnf) {
		
	}
     item = resize(item, count);
	return item;
	}
	
		private static ShelvedItem[] resize(ShelvedItem[] data, int size) {
			ShelvedItem[] temp = new ShelvedItem[size];
			int limit = data.length > size ? size : data.length;
			for (int i=0; i<limit; i++) {
				temp[i] = data[i];
			}
			
			return temp;
	}
		
		
		
       // Creating menu with different options for user
		
		public static ShelvedItem[] menu(Scanner response, ShelvedItem[] data, ArrayList<String> shopcart) {
			boolean done = false;
			// Menu for user to choose
			do {
				System.out.println("1. Create and add item");
				System.out.println("2. Search for item");
				System.out.println("3. Remove items");
				System.out.println("4. Update items");
				System.out.println("5. Exit");
				System.out.print("Choice: ");
				String choice = response.nextLine();
				
				switch (choice) {
					case "1": // Add item
						 createItem(data, response);
						break;
					case "2": // Search for item
						findItem(data, response);
						break;
					case "3": // update item (change Item information)
						removeItem(data, response, shopcart);
					    break;	
						
					case "4": // update item (change Item information)
						modifyItem(data, response);
					    break;
					
					case "5": // Exit the program
						done = true;
						break;
					default:  // In case the user enters input of incorrect format
						System.out.println("Sorry the input is not valid.");
				}        
			} while (!done);
			
			return data;
		}
		
		
		
		
			
		
		// Option 1: Create new item

		public static ShelvedItem[] createItem(ShelvedItem[] data, Scanner response) {
			
			
			// Firstly, I prompt the user for the 2 main information: the name and price
			
			ShelvedItem a;
			try {
			System.out.print("Name of item: ");
			String name = response.nextLine();
			System.out.print("Item price: ");
			double price = 0;
		
			try {    // Try catch for error check
				price = Double.parseDouble(response.nextLine());
			} catch (Exception e) {
				System.out.println("That is not valid. Returning to menu.");
				return data;
			}
			
			
// Next, I continue to ask user if the user wanted to create a shelved item, a produced item or age restricted item
			
			System.out.print("Type of Item (shelveditem, produceditem, or ageresitem): ");
			String type = response.nextLine();
			switch (type.toLowerCase()) {  // Switch case
				case "shelveditem":  // For this case, only name and price are needed
					
					a = new ShelvedItem(name, price);
					System.out.println("Item created "); // Notify user
					System.out.println(a); // Print out new item
					break;
					
					
					
				case "produceditem": // For this case, name, price, and expiration date needed
					System.out.print("enter expiration date in format mm-dd-yyyy: ");
					
					String edate = response.nextLine();
					
	// Item with produced item format will be created and print out to console for user to verify					
					a = new ProducedItem(name, price, edate);
					System.out.println("Item created ");// Notify user
					System.out.println(a); // Print out new item
					break;
					
					
					// For this case, the user need to enter the restricted age
				case "ageresitem":
                     System.out.print("ENter restricted age: ");
					
					int age = Integer.parseInt(response.nextLine());
					
// Item with age-restricted format will be created and print out to console for user to verify
					a = new AgeResItem(name, price, age);
					System.out.println("Item created "); // Notify user
					System.out.println(a); // Print out new item
					break;
					
					
					// If user enter any item type other than the 3 above, error message will appear
					
				default:  
					System.out.println("This type of item is not available. Returning to menu.");
					return data;
			}
			
			data = resize(data, data.length+1);
			data[data.length - 1] = a;
			
			return data;  // If the user enter input that is not the right format
		} catch (Exception e) {  
			System.out.println("Invalid input, please enter again");
		}
	
		return data;
	}
		
		
		
		// Option 2: Search for item
		
		public static void findItem(ShelvedItem[] data, Scanner response) {	
			
   // Prompt user for item name to search
		try {	System.out.println("Enter name of item you want to search");
			String enter = response.nextLine();
			int index = -1;
		// Search through all the list to find items of the same input name		
			for (int i2=0; i2 < data.length; i2++)
			{ if(enter.equalsIgnoreCase(data[i2].getName()))
			{   index=i2;
				System.out.println(data[i2].toString());
				
	// Print out all the items with the same name for user to know the number of that item left
			}
			
			}
		
		   }
		catch(Exception e) {
			System.out.println("Item not available!");
		}
		} // Try catch is used so that if user enter items that are not in the store,
		  // an error message will appear
		
		
		
		
		
		// Option 3: Method to remove item and add to shop cart
 private static int removeItem(ShelvedItem[] data, Scanner response, ArrayList<String> shopcart) {
	boolean done = false;	
			
do {	// prompt user item that user want to remove
	System.out.println ("Enter item you want to remove:");
	String rev = response.nextLine();
	int index = -1;
	
	
// Search through the file for the desired item
	for(int i2 = 0; i2 < data.length; i2++)
	{
		if(rev.equalsIgnoreCase(data[i2].getName()))
		{
			index = i2;
			shopcart.add(data[i2].toString());
			System.out.println ("Item you removed: " + data[i2].toString());
			break; // Notify user the item that is removed
		}
	}
	
int j = 0;

// Create a new array and copy all the data from all array except for the data chosen by user
ShelvedItem[] copy = new ShelvedItem[data.length - 1];
for (int i3 = 0; i3 < data.length; i3++) {
	if(index !=i3) {
		copy[j] = data[i3];
		ShelvedItem item = copy [j];
		System.out.println(copy[j]);
		j++; // print out the copied new array
	}
}

	if(index == -1) { // Notify user if item does not exist
		System.out.println("Item not found");
	}
	
	// Notify user the items that are inside the shop cart
	System.out.println("Product added to cart: " + shopcart);
	
	System.out.println("Do you want to continue? (press y for yes or 'n' for no): ");
	String rep = response.nextLine();
	if(rep.equalsIgnoreCase("y")) { // If user choose 'y' continue to prompt user to remove item
		done = true;
	}
	else { // End if user choose 'n'
		done = false;
	}
 } while(done);
 return -1;
}
		
	
		
			
		
	// Option 4: Change item information
		
		public static void modifyItem(ShelvedItem[] data, Scanner response) {
			
			// Prompt user for item that needed to change information
			System.out.println("Enter item that you want to make adjustments: ");
			String change1 = response.nextLine();
			int index = -1;
			try {
			for(int i1=0; i1 <= data.length; i1++)
			{  // Search and print out the item needed to change so that the user can compare later on
		     if(change1.equalsIgnoreCase(data[i1].getName()));
		     { 
		    	 System.out.println(data[i1].toString());
		     
		     index = i1;
		     }
		     break;}
			           // Prompt user if need to change item name or price
			System.out.println("You want to change price (enter p) or name (enter n): ");
			String change2 = response.nextLine();
			switch(change2) {
		
			
			// If user wanted to change the price of the searched item
			case "p":  
				System.out.println("Enter new price to change ");
				double newprice = Double.parseDouble(response.nextLine());
				data[index].setPrice(newprice);
				System.out.println("Name: " + data[index].getName() + ", New price: " + data[index].getPrice());
				break; // Present the Item with new price for user to see
				
			// If user need to change the name of the searched item
			case "n": 
			System.out.println("Enter new item name: ");
			String newname = response.nextLine();
			data[index].setName(newname);
			System.out.println("New name: " + data[index].getName() + ", Price: " + data[index].getPrice());
			break; // Present the Item with new name for user to see
			}
			}catch(Exception e) {
				System.out.println("Invalid input");
			}
		}


			
		
		
				
		// For reading the file (user type in the file path)
		public static void main(String[] args) {
			
			ArrayList<String>shopcart = new ArrayList<String>();
			
			// Create an ArrayList for use
			
			// Create new scanner
			Scanner input = new Scanner(System.in);
// prompt user for file path to load file
			System.out.print("Load file (example path:src/project/stock.csv): ");
			String filename = input.nextLine();
			// Load file (if exists)
			ShelvedItem[] data = readFile(filename);
			// Generate menu
			data = menu(input, data, shopcart);
			// Save file
			
			input.close();
			System.out.println("Ended!");
		} // If user choose to exit, notify user that the program has 'ended!'
		
		
		
				

}
	
	
