package project;

public class ProducedItem extends ShelvedItem { // Constructor of Produced Item

	
	
	// Basic methods
	
	
	  public String date;
	   
	   
	   public ProducedItem() {
			
			this.date = "";
			
			
		}	
	   
	   
	   public ProducedItem(String name, double price, String date) {
		   super(name,price);
		       this.date= date;
			}
			   
			public void setDate(String date) {
				   this.date = date;
				}
			   
			   
			   public String getDate() {
				  return date;}
				
			   
			   
				
			  	
				
			   // toString format that will appear on console
				@Override
				public String toString() {
		return "Name: " + getName() + ", price: " + getPrice() + ", Expiration date: " + date;
				}
				  

}
