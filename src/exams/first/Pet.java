package exams.first;

import labs.lab4.GeoLocation;

public class Pet {

	private String name;
 private int age;
 
 
//Default constructor which takes name and age as arguments.
 public Pet()
	{   this.name = "";
	   this.age = 0;
	
	}
 
//non-default constructor which takes name and age as arguments.
 public Pet(String name, int age) {
     
     this.name = name;
     this.age= age;
 }
  // For name:
	   //accessor method 
	   public String getName() {
	       return name;
	   }
	   //mutator method 
	   public void setName(String name) {
	       this.name = name;
	   }
	  // For age
	   //Accessor method 
	   public int getAge() {
	       return age;
	   }
	  
	   //mutator method 
	   public void setAge(int age) {
	       this.age = age;
	   }
	  
	   //toString() method . It is used to print the object in desired format
	   @Override
	   public String toString() {
	       return "Name of pet: "+ name + ", age: " + age + ".";
	   }
	 
	  
	   //checks age whether it is positive and return true if it is.
	   public boolean validAge()
	   {
	       if(this.age>=1 )
	           return true;
	       return false;
	   }
       //(the equals method).
	   @Override
	   public boolean equals(Object obj) {
		   if (!(obj instanceof Pet)) {
			   return false;
		   }
		   else {
		   Pet p = (Pet)obj;
		   if(this.name ==(p.getName())&& this.age ==p.getAge())
			   return true;
		   else {
			   return false;}
		   }
		   }
 
 
 
}
