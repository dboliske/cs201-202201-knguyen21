package exams.first;

import java.util.Scanner;

public class KhoiNguyenQuestion3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int size;

	        Scanner input = new Scanner(System.in);
	        // Create scanner for user to input information
	        
	        
	        System.out.print("Enter triangle's size (no.of patterns per side) : ");
	        size = input.nextInt();

	        input.close(); // Close the scanner
	        
	        for(int i=1; i<= size; i++) {       
	            
	            System.out.println();

	            if( i==1 || i == size) { // fill the top row (i== 1) and bottom row (i== size) with  pattern
	      
	                for(int j=1; j <= size; j++){
	                
	                    System.out.print("*" + " ");
	           // Print the  pattern for the entire row     
	                }
	            } 
	              else { 
	                // This is for the middle part of the triangle (all rows and columns except for top and bottom row)
	                for(int k= 1; k<= size;k++) { 
	                
	                   
	                    	System.out.print("*" + " ");
	              // This is to print the pattern at the first and last position of k      
	                      
	                           
	                   
	                }
	          }
	        }
	}

}
