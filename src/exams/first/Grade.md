# Midterm Exam

## Total

83/100

## Break Down

1. Data Types:                  17/20
    - Compiles:                 5/5
    - Input:                    2/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  16/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               1/5
4. Arrays:                      15/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  0/5
5. Objects:                     15/20
    - Variables:                5/5
    - Constructors:             3/5
    - Accessors and Mutators:   3/5
    - toString and equals:      4/5

## Comments

1. The program only reads in a single digit, not any integer.
2. Program technically works, but if-else structure could be much better written.
3. Doesn't print a triangle.
4. Doesn't do any counting of the words entered.
5. Non-default constructor and mutator methods do not validate `age` and the `equals` method does not properly compare `names`.
