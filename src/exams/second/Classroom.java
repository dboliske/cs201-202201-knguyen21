package exams.second;

public class Classroom {

	// Create variables
		protected String building;
		protected String roomNumber; 
		private int seats;
		 
		
		// Using different methods
		public Classroom() {
			
			this.building = "";
			this.roomNumber = "";
			this.seats = 1;
		}
		  
		public Classroom(String building, String roomNumber, int seats) {
	    
	       this.building= building;
	       this.roomNumber = roomNumber;
		   this.seats = seats;
		   
		}
		   
		public void setBuilding(String building) {
			   this.building = building;
			}
		   
		//accessor method    
		   public String getBuilding() {
			  return building;
			}
		   
		   
		   public void setRoomNumber(String roomNumber) {
			   this.roomNumber = roomNumber;
			}
		 //accessor method    
		   public String getRoomNumber() {
			   return roomNumber;
		   }
		   
		   
			
		   public void setSeats(int seats) {
			if(seats>0) {
			   this.seats = seats;
			
			}
		   
		   }
		   public int getSeats() {
			   return seats;
		   }
			
			
		   // toString format
			@Override
			public String toString() {
	return "Building name: " + building +", room number: " + roomNumber + ", seats: " + seats ;
			}
			
			
			
	}
