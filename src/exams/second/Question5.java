package exams.second;

import java.util.Scanner;

public class Question5
{
	// Main program
 public static void main(String [ ] args)
    {
        double arr[] = { 0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
        Scanner enter= new Scanner(System.in); 
        System.out.print("Enter a number to search: "); // Prompt user for number
        Double numsearch= Double.parseDouble(enter.nextLine());        
        
        
  
        int index = JumpSearch(arr, numsearch);
 
        // Print the index where 'numsearch' is located
        System.out.println("Number " + numsearch +
                            " is found at index " + index);
    }
  
  
 
 // Method for jump search
 public static int JumpSearch(double[] numArr, double numsearch)
    {
      int x = numArr.length;
 
        
      int step = (int)Math.floor(Math.sqrt(x));
 
        
      int prev = 0;
      while (numArr[Math.min(step, x)-1] < numsearch)
        {
          prev = step;
          step += (int)Math.floor(Math.sqrt(x));
            if (prev >= x)
                return -1;
        }
 
        
     while (numArr[prev] < numsearch)
        {
            prev++;
 
   // Return -1 if the element cannot be found after going through the whole array
   
    if (prev == Math.min(step, x))
                return -1;
        }
 
        // Once element is found
        if (numArr[prev] == numsearch)
            return prev;
 
        return -1;
    }
 
}

