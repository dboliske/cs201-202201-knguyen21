package exams.second;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Question3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 boolean confirm = true;
  Scanner input = new Scanner(System.in);
  ArrayList<Double> numbers = new ArrayList<Double>(); // Creating ArrayList
  
 do {
	System.out.print("Enter a number:");
	Double answer = Double.parseDouble(input.nextLine());
	numbers.add(answer);
	System.out.println(numbers);
	System.out.println("Continue entering number? (enter 'no' to exit, 'yes' to continue): ");
	String choice = input.nextLine();
	if(choice.equalsIgnoreCase("no")){
		confirm = true;
	}
	if(choice.equalsIgnoreCase("yes")) {
	    confirm = false;
	}
	else {
		confirm = true;
	}
	
	
 } while(!confirm); 
  
 System.out.println("Min number is: " + Collections.min(numbers)); 
 System.out.println("Max number is: " + Collections.max(numbers));  
  
	}

}
