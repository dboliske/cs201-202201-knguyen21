package exams.second;



public abstract class Polygon {

	// Create variables
	protected String name;
	
	 
	
	// Using different methods
	public Polygon() {
		
		this.name = "";
		
	}
	  
	public Polygon(String name) {
    
      this.name = name;
	   
	}
	   
	public void setName(String name) {
		   this.name = name;
		}
	   
	   
	   public String getName() {
		  return name;
		}
	   
public abstract double area();
public abstract double perimeter();	  
	   
	   
	   
	   
	   
		
		
	   // toString format
		@Override
		public  String toString() {
		return "Polygon";
	}

		
		
		
}

