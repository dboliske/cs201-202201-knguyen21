package exams.second;

public class Circle extends Polygon{

	private double radius;
	
	public Circle() {
		
		this.radius = 1;
		
		
		}
		  
		public Circle(String name, double radius) {
	    super(name);
	      this.radius= radius;
		   
		}
		   
		public void setRadius(double radius) {
			if(radius>0) {
				   this.radius = radius;
				
				}
			   
			}
		   
		//accessor method    
		public double getRadius() {
			  return radius;
			}
		   
		   
		   
		
		   	   
		   
		   public double area() {

		        double circleArea = Math.PI * radius * radius;

		        return circleArea;

		    }   
		  
		   
		   public double perimeter() {

		        double circlePeri =  2.0 * Math.PI * radius;

		        return circlePeri;

		    }   
		   
		   
		   
			
			
		   // toString format
			@Override
			public String toString() {
return "Name: " + getName() + "radius" + radius + ", Area: " + (Math.PI * radius * radius) + ", Perimeter: " + (2.0 * Math.PI * radius);
						}

}