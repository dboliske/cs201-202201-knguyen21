package exams.second;



public class ComputerLab extends Classroom {

	
		// Create variables
			
			private boolean computers;
			
			
			// Using different methods
			public ComputerLab() {
				
				
				this.computers = false;
				
			}
			  
			public ComputerLab(String building, String roomNumber, int seats, boolean computers) {
		    super(building, roomNumber, seats);
		       
			   this.computers= computers;
			   
			}
			   
			
			   
				
			   public void setComputers(boolean computers) {
					this.computers= computers;
				
				}
			   
			   
			   public boolean hasComputers() {
					return computers;
				}	
				
			   public String toString() {
					return "Building name: " + building +", room number: " + roomNumber + ", seats: " + getSeats() + ", computer availability: " + computers  ;
							}
							
							
							
					}
			
			   
				
				
			   