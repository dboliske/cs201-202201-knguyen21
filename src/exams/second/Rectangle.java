package exams.second;

public class Rectangle extends Polygon {

	private double width;
	private double height;
	
	public Rectangle() {
		
		this.width = 1;
		this.height= 1;
		
		}
		  
		public Rectangle(String name, double width, double height) {
	    super(name);
	      this.width = width;
	      this.height = height;
		   
		}
		   
		public void setWidth(double width) {
			if(width>0) {
				   this.width = width;
				
				}
			   
			}
		   
		   
		public double getWidth() {
			  return width;
			}
		   
		   
		   
		public void setHeight(double height) {
				if(height>0) {
					   this.height = height;
					
					}
				   
				}
			   
			   
		public double getHeight() {
				  return height;
				} 
		   
		   
		   
		   
		   
		   
		   public double area() {

		        double rectArea = height * width;

		        return rectArea;

		    }   
		  
		   
		   public double perimeter() {

		        double rectPeri =  2.0 * (height + width);

		        return rectPeri;

		    }   
		   
		   
		   
			
			
		   // toString format
			@Override
			public String toString() {
	return "Name: " + getName() + "width: " + width +", height: " + height + ", Area: " + (height * width) + ", Perimeter: " + (2.0 * (height + width));
			}
			
				
		
		
	}
	
	


